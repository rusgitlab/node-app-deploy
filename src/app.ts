import express from 'express'
import booksRoutes from './routes/books'

const app = express()

app.use(express.json())

app.use('/books', booksRoutes)

// const x = async () => {
//   console.log('async')
//   throw Error('test err 1')
//   return 123 
// }


// try {
//   console.log(x().then(
//     (res) => {
//       console.log(res)
//     },
//     (err) => {
//       console.log('Caught error 1' + err.message)
//     },
//   ))
// } catch(err) {
//   console.log('Caught error 2' + err.message)
// }


app.listen(3000, () => {
  console.log('Listening...')
})


// import crypto from 'crypto'