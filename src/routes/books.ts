import { Router } from 'express'
import { getBooks, saveBook } from '../controllers/BooksController'

const router = Router()

router.get('/', getBooks)
router.post('/', saveBook)
// router.patch('/:id')
// router.delete('/:id')

export default router