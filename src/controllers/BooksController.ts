import { RequestHandler } from 'express'
import { Book } from '../models/book'

const books: Book[] = []

books.push(new Book((Math.random() + 1).toString(36).substring(7), 'Book1'))
books.push(new Book((Math.random() + 1).toString(36).substring(7), 'Book2'))

export const getBooks: RequestHandler = (req, res, next) => {
  res.json(books)
}

export const saveBook: RequestHandler = (req, res, next) => {
  const bookName = (req.body as {name: string}).name
  const newBook = new Book((Math.random() + 1).toString(36).substring(7), bookName)
  books.push(newBook)
  res.json(newBook)
}

export const updateBook: RequestHandler<{id: string}> = (req, res, next) => {
  res.json({})
}
